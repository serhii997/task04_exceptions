package com.movchan.controller;

import com.movchan.model.MyCustomException;

public interface Controller {
    void printInformation();
    void checkOld()throws MyCustomException;
}
