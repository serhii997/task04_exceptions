package com.movchan.controller;

import com.movchan.model.MyCustomException;
import com.movchan.model.Person;

public class ControllerImpl implements Controller {
    private Person person;

    public ControllerImpl() {
    }

    public ControllerImpl(Person person) {
        this.person = person;
    }


    @Override
    public void printInformation() {
        System.out.println(person.toString());
    }

    @Override
    public void checkOld() throws MyCustomException {
        if(person.getOld() < Person.MAJORITY){
            throw new MyCustomException("This young people");
        }else {
            System.out.println("this people can voting");
        }
    }
}
