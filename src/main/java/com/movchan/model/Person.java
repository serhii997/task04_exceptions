package com.movchan.model;

import java.util.Objects;

public class Person {
    public static final int MAJORITY = 18;
    private String firstName;
    private String surName;
    private int old;

    public Person(String firstName, String surName, int old) throws Exception {

        if(old <= 18){
            throw new MyCustomException("This person is not allowed to vote ");
        }

        this.firstName = firstName;
        this.surName = surName;
        this.old = old;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSurName() {
        return surName;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    public int getOld() {
        return old;
    }

    public void setOld(int old) {
        this.old = old;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return old == person.old &&
                Objects.equals(firstName, person.firstName) &&
                Objects.equals(surName, person.surName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstName, surName, old);
    }

    @Override
    public String toString() {
        return "Person{" +
                "firstName='" + firstName + '\'' +
                ", surName='" + surName + '\'' +
                ", old=" + old +
                '}';
    }
}
