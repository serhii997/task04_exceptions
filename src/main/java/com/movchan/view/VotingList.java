package com.movchan.view;

import com.movchan.controller.Controller;
import com.movchan.controller.ControllerImpl;
import com.movchan.model.MyCustomException;
import com.movchan.model.Person;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class VotingList {
    private Controller controller;
    private Map<String, String> menu;
    private Map <String, Printable> functionMenu;

    private static Scanner sc = new Scanner(System.in);

    public VotingList() {
        controller = new ControllerImpl();
        menu = new LinkedHashMap<String, String>();
        menu.put("1", "   1 - Add new Person");
        menu.put("2", "   2 - Print Person");
        menu.put("3", "   3 - Check person");
        menu.put("Q", "   Q - exit");

        functionMenu = new LinkedHashMap<String, Printable>();

        functionMenu.put("1", this::button1);
        functionMenu.put("2", this::button2);
        functionMenu.put("3", this::button3);
    }

    private void button1() {

        try {
            controller = new ControllerImpl(registrationPerson());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Person registrationPerson() throws Exception {
        System.out.println("Please input First name");
        String name = sc.next();
        System.out.println("Please input surname");
        String surname = sc.next();
        System.out.println("Please input old person");
        int old = 0;
        try {
            old = sc.nextInt();
        }catch (NumberFormatException e){
            System.err.println(e.getMessage());
        }
        return new Person(name, surname, old);
    }

    private void button2(){
        controller.printInformation();
    }

    private void button3() {
        try {
            controller.checkOld();
        } catch (MyCustomException e) {
            e.printStackTrace();
        }
    }

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = sc.nextLine().toUpperCase().trim();
            try {
                functionMenu.get(keyMenu).print();
            } catch (Exception e) {
                System.out.println("Command not found");
            }
        } while (!keyMenu.equals("Q"));
    }
}
