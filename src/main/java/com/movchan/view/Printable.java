package com.movchan.view;

import com.movchan.model.MyCustomException;

public interface Printable {
    void print() throws MyCustomException;
}
